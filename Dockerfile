FROM debian:latest

RUN apt-get update
RUN apt-get install -y ruby-full rails

# Generate a new project
# ----------------------
# Use it only if you don't have a Ruby on Rail project on your pc.
# First, decomment the following :
RUN mkdir /myApp
WORKDIR /myApp

CMD rails server

EXPOSE 3000